// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package models

type Driver struct {
	ID       uint64 `db:"ID"`
	Name     string
	Email    string
	Password string
	Country  string
}

type Session struct {
	LKey   string
	Driver uint64
}

type Stat struct {
	ID     uint64 `db:"ID"`
	Driver uint64
	Key    string
	Value  float32
	Text   string
}
