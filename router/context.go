// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package router

import (
	"gitlab.com/sparkserver/carbonara/codec"
	"gitlab.com/sparkserver/carbonara/fesl"
)

type Context struct {
	Message *fesl.Message
	Conn    fesl.Conn
}

func (c *Context) Bind(r interface{}) error {
	return codec.Unmarshal(c.Message.Payload, r)
}

func (c *Context) Reply(r interface{}) error {
	pl, err := codec.Marshal(r)
	if err != nil {
		return err
	}
	return c.Conn.WriteFesl(&fesl.Message{
		Category: c.Message.Category,
		Seq:      c.Message.Seq,
		Payload:  pl,
	})
}
