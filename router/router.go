// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package router

import (
	"gitlab.com/sparkserver/carbonara/codec"
	"gitlab.com/sparkserver/carbonara/fesl"
)

type Handler = func(*Context) error
type Extractor = func(*fesl.Message) string

// CategoryExtractor uses message category to route messages
func CategoryExtractor(m *fesl.Message) string {
	return m.CategoryString()
}

// FieldExtractor uses a field from the message payload to route messages
func FieldExtractor(field string) Extractor {
	return func(m *fesl.Message) string {
		pl := codec.PayloadToMap(m.Payload)
		return pl[field]
	}
}

// NewRouter creates an empty Router with the specified extractor
func NewRouter(e Extractor) *Router {
	return &Router{
		routes:    make(map[string]Handler),
		extractor: e,
	}
}

// Router routes FESL messages based on output of the specified Extractor
type Router struct {
	routes    map[string]Handler
	extractor Extractor
	unhandled *Handler
}

// AddRoute adds a new route to the Router
func (r *Router) AddRoute(cat string, handler Handler) {
	r.routes[cat] = handler
}

// SetUnhandled sets the Handler that will be executed when no routes match the message
func (r *Router) SetUnhandled(handler Handler) {
	r.unhandled = &handler
}

// Handle handles FESL message
func (r *Router) Handle(c *Context) error {
	handler, ok := r.routes[r.extractor(c.Message)]
	if ok {
		return handler(c)
	}
	if r.unhandled != nil {
		(*r.unhandled)(c)
	}
	return nil
}
