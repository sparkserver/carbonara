// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package repo

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/sparkserver/carbonara/models"
)

func (r *Repository) GetDriverStatsByKeys(driver uint64, keys []string) ([]models.Stat, error) {
	query, args, err := sqlx.In("SELECT * FROM stats WHERE driver = ? AND `key` IN (?)", driver, keys)
	if err != nil {
		return nil, err
	}
	var stats []models.Stat
	err = r.db.Select(&stats, query, args...)
	return stats, err
}

func (r *Repository) UpdateStats(stats []models.Stat) error {
	_, err := r.db.NamedExec("INSERT INTO stats (driver, `key`, value, text) VALUES (:driver, :key, :value, :text) ON DUPLICATE KEY UPDATE value = VALUES(value), text = VALUES(text)", stats)
	return err
}
