// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package repo

import "database/sql"

// GetConfigString gets config value from database as a string.
// If there is no such key defined, an empty string is returned.
func (r *Repository) GetConfigString(key string) (string, error) {
	var value string
	err := r.db.QueryRow("SELECT value FROM settings WHERE `key` = ?", key).Scan(&value)
	if err == sql.ErrNoRows {
		return "", nil
	}
	return value, err
}
