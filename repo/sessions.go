// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package repo

import "gitlab.com/sparkserver/carbonara/models"

func (r *Repository) DeleteDriverSessions(driver uint64) error {
	_, err := r.db.Exec("DELETE FROM sessions WHERE driver = ?", driver)
	return err
}

func (r *Repository) CreateSession(session *models.Session) error {
	_, err := r.db.NamedExec("INSERT INTO sessions (lkey, driver) VALUES (:lkey, :driver)", session)
	return err
}

func (r *Repository) GetSession(lkey string) (*models.Session, error) {
	var session models.Session
	err := r.db.QueryRowx("SELECT * FROM sessions WHERE lkey = ?", lkey).StructScan(&session)
	if err != nil {
		return nil, err
	}
	return &session, nil
}
