// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package repo

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/sparkserver/carbonara/config"
)

type Repository struct {
	db *sqlx.DB
}

func New(db *sqlx.DB) Repository {
	return Repository{
		db: db,
	}
}

func NewFromConfig(config *config.Config) (Repository, error) {
	db, err := sqlx.Open("mysql", config.DSN)
	if err != nil {
		return Repository{}, err
	}
	return New(db), nil
}
