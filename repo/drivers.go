// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package repo

import "gitlab.com/sparkserver/carbonara/models"

func (r *Repository) CreateDriver(driver *models.Driver) error {
	_, err := r.db.NamedExec("INSERT INTO drivers (name, email, password, country) VALUES (:name, :email, :password, :country)", driver)
	return err
}

func (r *Repository) GetDriver(id uint64) (*models.Driver, error) {
	var driver models.Driver
	err := r.db.QueryRowx("SELECT * FROM drivers WHERE ID = ?", id).StructScan(&driver)
	if err != nil {
		return nil, err
	}
	return &driver, nil
}

func (r *Repository) GetDriverByName(name string) (*models.Driver, error) {
	var driver models.Driver
	err := r.db.QueryRowx("SELECT * FROM drivers WHERE name = ?", name).StructScan(&driver)
	if err != nil {
		return nil, err
	}
	return &driver, nil
}

func (r *Repository) GetDriverByEmail(email string) (*models.Driver, error) {
	var driver models.Driver
	err := r.db.QueryRowx("SELECT * FROM drivers WHERE email = ?", email).StructScan(&driver)
	if err != nil {
		return nil, err
	}
	return &driver, nil
}
