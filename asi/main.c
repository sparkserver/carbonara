/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include <Windows.h>
#include <stdio.h>
#include <ini.h>

FILE* logFile = NULL;
char feslHostOverride[64] = {0};

void logger_cb(void* ud, char* msg) {
    if (logFile != NULL) {
        fprintf(logFile, "%s", msg);
        fflush(logFile);
    }
}

typedef void (*initOptsOriginal)(int, int);

void initOptsHook(int opts, int unknown) {
    memcpy((void*)(opts+88), feslHostOverride, 64);
    ((initOptsOriginal)0x8e0f00)(opts, unknown);
}

int my_ini_handler(void* user, const char* section, const char* name, const char* value) {
    if (_stricmp(name, "FESLHostOverride") == 0) {
        strncpy(feslHostOverride, value, 63);
    }
    if (_stricmp(name, "DebugLogging") == 0 && strcmp(value, "1") == 0) {
        logFile = fopen("debug_log.txt", "w");
    }
    return 1;
}

void mem_write_int(size_t addr, int value) {
    DWORD oldProtect;
    VirtualProtect((void*)addr, 4, PAGE_EXECUTE_READWRITE, &oldProtect);
    *((int*)addr) = value;
    VirtualProtect((void*)addr, 4, oldProtect, &oldProtect);
}

BOOLEAN WINAPI DllMain(HINSTANCE hDllHandle, DWORD nReason, LPVOID Reserved) {
    if (nReason == DLL_PROCESS_ATTACH) {
        if (ini_parse("OnlineHelper.ini", my_ini_handler, NULL)) {
            MessageBoxA(NULL, "INI parsing failed!", "OnlineHelper", MB_ICONERROR);
            TerminateProcess(GetCurrentProcess(), 1);
        }
        
        // Overwrite networking subsystem's log message handler
        mem_write_int(0x8345c6, (int) logger_cb);
        
        // Overwrite CALL at networking options struct initialization
        mem_write_int(0x834682, ((int)initOptsHook) - 0x834682 - 4);
        
        // Bypass FESL certificate validation (from https://aluigi.altervista.org/patches/fesl.lpatch)
        DWORD oldProtect;
        VirtualProtect((void*)0x98053d, 5, PAGE_EXECUTE_READWRITE, &oldProtect);
        memcpy((void*)0x98053d, (char[]){0xb8, 0x15, 0x00, 0x00, 0x00}, 5);
        VirtualProtect((void*)0x98053d, 5, oldProtect, &oldProtect);
    }
    return TRUE;
}