CREATE TABLE `drivers` (
    `ID` SERIAL PRIMARY KEY,
    `name` varchar(16) NOT NULL UNIQUE,
    `email` varchar(255) NOT NULL UNIQUE,
    `password` varchar(255) NOT NULL,
    `country` char(2) NOT NULL
);
CREATE TABLE `settings` (
    `key` varchar(25) NOT NULL PRIMARY KEY,
    `value` varchar(255) NOT NULL
);
CREATE TABLE `sessions` (
    `lkey` char(16) NOT NULL PRIMARY KEY,
    `driver` SERIAL NOT NULL
);
CREATE TABLE `stats` (
    `ID` SERIAL NOT NULL PRIMARY KEY,
    `driver` SERIAL NOT NULL,
    `key` VARCHAR(255) NOT NULL,
    `value` FLOAT NOT NULL,
    `text` VARCHAR(255) NOT NULL
);