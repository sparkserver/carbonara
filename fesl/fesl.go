// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package fesl

import (
	"bytes"
	"encoding/base64"
	"encoding/binary"
	"encoding/hex"
	"fmt"
	"io"
	"strconv"

	"gitlab.com/sparkserver/carbonara/codec"
)

// Message represents a FESL message.
type Message struct {
	Category [4]byte
	Seq      uint32
	Payload  string
}

type msgHeader struct {
	Category [4]byte
	Seq      uint32
	Length   uint32
}

// Bytes encodes Message into a byte slice.
func (m Message) Bytes() []byte {
	buf := new(bytes.Buffer)
	binary.Write(buf, binary.BigEndian, m.Category)
	binary.Write(buf, binary.BigEndian, m.Seq)
	payloadBytes := []byte(m.Payload)
	binary.Write(buf, binary.BigEndian, uint32(len(payloadBytes)+12+1))
	buf.Write(payloadBytes)
	buf.WriteByte(0)
	return buf.Bytes()
}

// CategoryString returns the Category field as string
func (m Message) CategoryString() string {
	return string(m.Category[:])
}

// Conn provides interface for reading and writing FESL messages.
type Conn interface {
	ReadFesl() (*Message, error)
	WriteFesl(msg *Message) error
}

// SimpleConn wraps io.ReadWriter and provides interface for reading and writing
// FESL messages.
type SimpleConn struct {
	io.ReadWriter
}

// ReadFesl reads one FESL message from the underlying reader.
func (c *SimpleConn) ReadFesl() (*Message, error) {
	var hdr msgHeader
	err := binary.Read(c, binary.BigEndian, &hdr)
	if err != nil {
		return nil, err
	}
	payload := make([]byte, int(hdr.Length-12))
	_, err = io.ReadFull(c, payload)
	if err != nil {
		return nil, err
	}
	return &Message{
		Category: hdr.Category,
		Seq:      hdr.Seq,
		Payload:  string(payload[:len(payload)-1]),
	}, nil
}

// WriteFesl writes a FESL message to the underlying writer.
func (c *SimpleConn) WriteFesl(msg *Message) error {
	cc, err := c.Write(msg.Bytes())
	fmt.Printf("OUT %v: %v\n", cc, hex.EncodeToString(msg.Bytes()))
	return err
}

type pendingMultipartMsg struct {
	data      []byte
	remaining int
}

// MultipartConn wraps FeslConn, adding support for message types and multipart messages.
type MultipartConn struct {
	SimpleConn
}

// ReadFesl reads one FESL message from the underlying reader.
// Multipart messages will be automatically combined into one message.
func (c *MultipartConn) ReadFesl() (*Message, error) {
	var pending *pendingMultipartMsg = nil
	for {
		msg, err := c.SimpleConn.ReadFesl()
		if err != nil {
			return nil, err
		}
		typ := msg.Seq >> 24
		seq := msg.Seq & 0x00FFFFFF
		msg.Seq = seq
		if typ == 0xB0 {
			payload := codec.PayloadToMap(msg.Payload)
			data, err := base64.StdEncoding.DecodeString(payload["data"])
			if err != nil {
				return nil, err
			}
			if pending != nil {
				pending.data = append(pending.data, data...)
				pending.remaining -= len(payload["data"])
				if pending.remaining == 0 {
					return &Message{
						Category: msg.Category,
						Seq:      msg.Seq,
						Payload:  string(pending.data),
					}, nil
				}
			} else {
				totalSize, err := strconv.Atoi(payload["size"])
				if err != nil {
					return nil, err
				}
				pending = &pendingMultipartMsg{
					data:      data,
					remaining: totalSize - len(payload["data"]),
				}
			}
			continue
		}
		return msg, nil
	}
}

// WriteFesl writes a FESL message to the underlying writer.
// Large messages will be automatically segmented into multiple multipart messages.
func (c *MultipartConn) WriteFesl(msg *Message) error {
	if len(msg.Payload) > 2048 {
		return c.writeMultipart(msg)
	}
	newMsg := *msg
	newMsg.Seq = typeSeq(0x80, newMsg.Seq)
	return c.SimpleConn.WriteFesl(&newMsg)
}

func (c *MultipartConn) writeMultipart(msg *Message) error {
	payload := []byte(msg.Payload)
	decodedSize := len(payload)
	encodedSize := b64len(decodedSize)
	offset := 0
	for offset < decodedSize {
		partLen := minInt(2048/4*3, decodedSize-offset)
		encoded := base64.StdEncoding.EncodeToString(payload[offset : offset+partLen])
		offset += partLen
		payload := fmt.Sprintf("data=%s\ndecodedSize=%d\nsize=%d", encoded, decodedSize, encodedSize)
		err := c.SimpleConn.WriteFesl(&Message{
			Category: msg.Category,
			Seq:      typeSeq(0xB0, msg.Seq),
			Payload:  payload,
		})
		if err != nil {
			return err
		}
	}
	return nil
}

func typeSeq(typ uint8, seq uint32) uint32 {
	return (seq & 0xFFFFFF) | (uint32(typ) << 24)
}

func b64len(len int) int {
	return (len + 3 - 1) / 3 * 4
}

func minInt(a, b int) int {
	if a < b {
		return a
	}
	return b
}
