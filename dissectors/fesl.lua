-- This Source Code Form is subject to the terms of the Mozilla Public
-- License, v. 2.0. If a copy of the MPL was not distributed with this
-- file, You can obtain one at https://mozilla.org/MPL/2.0/.

local proto = Proto("ea-fesl", "EA-FESL")
proto.fields.category = ProtoField.string("ea-fesl.category", "Category")
proto.fields.type = ProtoField.uint8("ea-fesl.type", "Type", base.HEX)

function base64decode(str)
    return ByteArray.new(str, true):base64_decode():raw()
end

function parse_payload(buffer)
    local payload_map = {}
    local payload = buffer:stringz()
    local i = 0
    for kv in string.gmatch(payload, "[^\n]+") do
        local j = string.find(kv, "=")
        payload_map[string.sub(kv, 1, j-1)] = string.sub(kv, j+1)
    end
    return payload_map
end

function proto.dissector(obuffer, pinfo, tree)
    local buffer = obuffer
    local remaining_bytes = buffer:len()
    while remaining_bytes > 0 do
        if buffer:len() < 12 then
            pinfo.desegment_len = 12 - buffer:len()
            return
        end
        local packet_size = buffer(8, 4):uint()
        if buffer:len() < packet_size then
            pinfo.desegment_len = packet_size - buffer:len()
            return
        end
        pinfo.cols.protocol = "EA-FESL"
        local subtree = tree:add(proto, buffer())
        subtree:add(proto.fields.category, buffer(0, 4))
        subtree:add(proto.fields.type, buffer(4, 1))
        subtree:add(buffer(5, 3), "Seq: " .. buffer(5,3):uint())
        subtree:add(buffer(8, 4), "Length: " .. packet_size)
        local payload_tree = subtree:add(buffer(12, packet_size - 12), "Packet data")
        local map = {}
        if packet_size > 12 then
            map = parse_payload(buffer(12, packet_size - 12))
        end
        for k, v in pairs(map) do
            payload_tree:add(k .. "=" .. v)
        end
        if buffer(4, 1):uint() == 0xb0 then
            local decoded = base64decode(map["data"])
            local mp_payload_tree = subtree:add(buffer(12, packet_size - 12), "Multipart payload")
            for kv in string.gmatch(decoded, "[^\n]+") do
                mp_payload_tree:add(kv)
            end
        end
        local info = "Category " .. buffer(0, 4):string()
        if map["TXN"] then
            info = info .. ", TXN " .. map["TXN"]
        end
        pinfo.cols.info = info
        remaining_bytes = remaining_bytes - packet_size
        if remaining_bytes > 0 then
            buffer = buffer(packet_size)
        end
    end
end

tcp_table = DissectorTable.get("tcp.port")
-- NFS Carbon
tcp_table:add(18210, proto)
tcp_table:add(18215, proto)
tcp_table:add(13505, proto)
tcp_table:add(18211, proto)

