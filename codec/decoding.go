// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package codec

import (
	"fmt"
	"reflect"
	"strconv"
	"strings"
)

func unescapeString(str string) string {
	if !strings.ContainsAny(str, "\"%") {
		return str
	}
	sb := strings.Builder{}
	ueSeq := ""
	ueState := false
	for _, ch := range str {
		if ch == '"' {
			continue
		} else if ch == '%' {
			ueState = true
		} else if ueState == true {
			ueSeq += string(ch)
			if len(ueSeq) == 2 {
				i, _ := strconv.ParseUint(ueSeq, 16, 10)
				sb.WriteRune(rune(i))
				ueSeq = ""
				ueState = false
			}
		} else {
			sb.WriteRune(ch)
		}
	}
	return sb.String()
}

func PayloadToMap(payload string) map[string]string {
	out := make(map[string]string)
	for _, line := range strings.Split(payload, "\n") {
		splits := strings.Split(line, "=")
		if len(splits) < 2 {
			continue
		}
		out[splits[0]] = unescapeString(splits[1])
	}
	return out
}

func Unmarshal(data string, value interface{}) error {
	return UnmarshalMap(PayloadToMap(data), value)
}

func UnmarshalMap(kvs map[string]string, value interface{}) (err error) {
	defer func() {
		if r := recover(); r != nil {
			err = fmt.Errorf("error while unmarshalling: %v", r)
		}
	}()
	d := decoder{
		kvs: kvs,
	}
	rValue := reflect.ValueOf(value)
	if rValue.Kind() != reflect.Ptr {
		return ErrNotStruct
	}
	rValue = rValue.Elem()
	if rValue.Kind() != reflect.Struct {
		return ErrNotStruct
	}
	d.decodeStruct(rValue)
	return nil
}

type decoder struct {
	kvs map[string]string
	key []string
}

func (d *decoder) pushKey(k string) {
	d.key = append(d.key, k)
}

func (d *decoder) popKey() {
	d.key = d.key[:len(d.key)-1]
}

func (d *decoder) keyString() string {
	return strings.Join(d.key, ".")
}

func (d *decoder) decodeStruct(s reflect.Value) {
	t := s.Type()
	for i := 0; i < t.NumField(); i++ {
		field := t.Field(i)
		d.decodeKeyValue(fieldName(field), s.Field(i))
	}
}

func (d *decoder) decodeKeyValue(key string, value reflect.Value) {
	d.pushKey(key)
	switch value.Kind() {
	case reflect.Struct:
		d.decodeStruct(value)
	case reflect.Slice:
		d.decodeSlice(value)
	default:
		d.decodeValue(value)
	}
	d.popKey()
}

func (d *decoder) decodeValue(value reflect.Value) {
	inValue := d.kvs[d.keyString()]
	switch value.Kind() {
	case reflect.String:
		value.SetString(unescapeString(inValue))
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64, reflect.Uintptr:
		i, _ := strconv.ParseUint(inValue, 10, 64)
		value.SetUint(i)
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		i, _ := strconv.ParseInt(inValue, 10, 64)
		value.SetInt(i)
	case reflect.Float32, reflect.Float64:
		f, _ := strconv.ParseFloat(inValue, 32)
		value.SetFloat(f)
	case reflect.Bool:
		if inValue == "1" {
			value.SetBool(true)
		} else {
			value.SetBool(false)
		}
	default:
		panic("can't decode " + value.Kind().String())
	}
}

func (d *decoder) decodeSlice(s reflect.Value) {
	d.pushKey("[]")
	len, _ := strconv.ParseUint(d.kvs[d.keyString()], 10, 64)
	d.popKey()
	slice := reflect.MakeSlice(s.Type(), int(len), int(len))
	for i := 0; i < int(len); i++ {
		d.decodeKeyValue(strconv.Itoa(i), slice.Index(i))
	}
	s.Set(slice)
}
