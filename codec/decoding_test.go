// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package codec

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDecode(t *testing.T) {
	payload := "hello.[]=1\nhello.0=test\n"
	var val struct {
		Test []string `fesl:"hello"`
	}
	err := Unmarshal(payload, &val)
	assert.Nil(t, err)
	assert.Equal(t, 1, len(val.Test))
}
