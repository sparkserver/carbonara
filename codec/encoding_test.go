// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package codec

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNotStruct(t *testing.T) {
	_, err := Marshal("hello")
	assert.Equal(t, ErrNotStruct, err)
}

func TestEncode(t *testing.T) {
	str, err := Marshal(struct {
		Slice  []string
		BoolT  bool
		BoolF  bool
		Uint   uint32
		Float  float32
		String string
		Map    map[string]interface{}
	}{
		Slice:  []string{"test"},
		BoolT:  true,
		BoolF:  false,
		Uint:   3,
		Float:  1.23,
		String: "Hello world",
		Map: map[string]interface{}{
			"keya": []string{"value0", "value1", "value2"},
			"keyb": 123,
		},
	})
	assert.Nil(t, err)
	lines := strings.Split(str, "\n")
	assert.ElementsMatch(t, []string{
		"Slice.[]=1",
		"Slice.0=test",
		"BoolT=1",
		"BoolF=0",
		"Uint=3",
		"Float=1.23",
		"String=\"Hello world\"",
		"Map.{}=2",
		"Map.{keya}.[]=3",
		"Map.{keya}.0=value0",
		"Map.{keya}.1=value1",
		"Map.{keya}.2=value2",
		"Map.{keyb}=123",
	}, lines[:len(lines)-1])
}
