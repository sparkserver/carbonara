// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package codec

import (
	"errors"
	"fmt"
	"reflect"
	"strconv"
	"strings"
)

var ErrNotStruct = errors.New("only structs can be (un)marshalled")

func fieldName(f reflect.StructField) string {
	tag := f.Tag.Get("fesl")
	if tag != "" {
		return tag
	}
	return f.Name
}

func containsRune(str string, r rune) bool {
	for _, ch := range str {
		if ch == r {
			return true
		}
	}
	return false
}

func encodeString(sb *strings.Builder, val string) {
	quoted := false
	if strings.ContainsAny(val, ", ") {
		sb.WriteRune('"')
		quoted = true
	}
	for _, ch := range val {
		if containsRune(" -.,/0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz", ch) {
			sb.WriteRune(ch)
		} else {
			sb.WriteRune('%')
			if ch < 0x10 {
				sb.WriteRune('0')
			}
			sb.WriteString(strconv.FormatUint(uint64(ch), 16))
		}
	}
	if quoted {
		sb.WriteRune('"')
	}
}

func Marshal(value interface{}) (_ string, err error) {
	defer func() {
		if r := recover(); r != nil {
			err = fmt.Errorf("error while marshalling: %v", r)
		}
	}()
	e := encoder{}
	rValue := reflect.ValueOf(value)
	if rValue.Kind() != reflect.Struct {
		return "", ErrNotStruct
	}
	e.encodeStruct(rValue)
	return e.sb.String(), nil
}

type encoder struct {
	sb  strings.Builder
	key []string
}

func (e *encoder) pushKey(k string) {
	e.key = append(e.key, k)
}

func (e *encoder) popKey() {
	e.key = e.key[:len(e.key)-1]
}

func (e *encoder) keyString() string {
	return strings.Join(e.key, ".")
}

func (e *encoder) encodeStruct(s reflect.Value) {
	t := s.Type()
	for i := 0; i < t.NumField(); i++ {
		field := t.Field(i)
		e.encodeKeyValue(fieldName(field), s.Field(i))
	}
}

func (e *encoder) encodeKeyValue(key string, value reflect.Value) {
	e.pushKey(key)
	switch value.Kind() {
	case reflect.Struct:
		e.encodeStruct(value)
	case reflect.Array, reflect.Slice:
		e.encodeArray(value)
	case reflect.Map:
		e.encodeMap(value)
	default:
		e.sb.WriteString(e.keyString())
		e.sb.WriteRune('=')
		e.encodeValue(value)
		e.sb.WriteRune('\n')
	}
	e.popKey()
}

func (e *encoder) encodeValue(value reflect.Value) {
	switch value.Kind() {
	case reflect.String:
		encodeString(&e.sb, value.String())
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64, reflect.Uintptr:
		e.sb.WriteString(strconv.FormatUint(value.Uint(), 10))
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		e.sb.WriteString(strconv.FormatInt(value.Int(), 10))
	case reflect.Float32, reflect.Float64:
		e.sb.WriteString(strconv.FormatFloat(value.Float(), 'f', -1, 32))
	case reflect.Bool:
		if value.Bool() {
			e.sb.WriteRune('1')
		} else {
			e.sb.WriteRune('0')
		}
	default:
		panic("can't encode " + value.Kind().String())
	}
}

func (e *encoder) encodeArray(s reflect.Value) {
	e.encodeKeyValue("[]", reflect.ValueOf(s.Len()))
	for i := 0; i < s.Len(); i++ {
		e.encodeKeyValue(strconv.Itoa(i), s.Index(i))
	}
}

func (e *encoder) encodeMap(s reflect.Value) {
	keyCount := 0
	for _, key := range s.MapKeys() {
		e.encodeKeyValue("{"+key.String()+"}", reflect.ValueOf(s.MapIndex(key).Interface()))
		keyCount++
	}
	e.encodeKeyValue("{}", reflect.ValueOf(keyCount))
}
