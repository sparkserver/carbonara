// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package main

import (
	"log"
	"net"

	"gitlab.com/sparkserver/carbonara/cmd/messagingd/routes"
	"gitlab.com/sparkserver/carbonara/fesl"
	"gitlab.com/sparkserver/carbonara/router"
)

func main() {
	listener, err := net.Listen("tcp", ":13505")
	if err != nil {
		log.Fatalf("Failed to listen: %v", err)
	}

	router := routes.NewRouter()

	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Fatalf("Failed to accept: %v", err)
		}
		feslConn := &fesl.SimpleConn{conn}
		go handleConn(router, feslConn)
	}
}

func handleConn(r *router.Router, conn fesl.Conn) {
	for {
		msg, err := conn.ReadFesl()
		if err != nil {
			log.Printf("Failed to read a message: %v", err)
			return
		}
		log.Printf("IN: %#v", msg)

		ctx := &router.Context{
			Message: msg,
			Conn:    conn,
		}
		r.Handle(ctx)
	}
}
