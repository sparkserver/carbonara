// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package routes

import (
	"log"

	"gitlab.com/sparkserver/carbonara/router"
)

func NewRouter() *router.Router {
	r := router.NewRouter(router.CategoryExtractor)
	r.AddRoute("AUTH", Authenticate)
	r.AddRoute("RGET", RosterGet)
	r.AddRoute("EPGT", EPGT)
	r.AddRoute("PSET", PresenceSet)
	r.AddRoute("PING", Ping)

	r.SetUnhandled(func(c *router.Context) error {
		log.Printf("Unknown message category: %v", c.Message.CategoryString())
		return nil
	})
	return r
}

type authResponse struct {
	ID   int    `fesl:"ID"`
	USER string `fesl:"USER"`
	TIID int    `fesl:"TIID"`
	TITL string `fesl:"TITL"`
}

func Authenticate(c *router.Context) error {
	return c.Reply(authResponse{
		ID:   1,
		USER: "yolo@messaging.ea.com/eagames/NFS-2007",
		TIID: 0,
		TITL: "Need for Speed Carbon",
	})
}

type rgetRequest struct {
	ID int `fesl:"ID"`
}

type rgetResponse struct {
	ID   int `fesl:"ID"`
	SIZE int `fesl:"SIZE"`
}

func RosterGet(c *router.Context) error {
	var req rgetRequest
	if err := c.Bind(&req); err != nil {
		return err
	}
	return c.Reply(rgetResponse{
		ID:   req.ID,
		SIZE: 0,
	})
}

type pingResponse struct{}

func Ping(c *router.Context) error {
	return c.Reply(pingResponse{})
}

type epgtResponse struct {
	ID   int    `fesl:"ID"`
	ADDR string `fesl:"ADDR"`
	ENAB string `fesl:"ENAB"`
}

func EPGT(c *router.Context) error {
	var req rgetRequest
	if err := c.Bind(&req); err != nil {
		return err
	}
	return c.Reply(epgtResponse{
		ID:   req.ID,
		ADDR: "whatever",
		ENAB: "F",
	})
}

type psetResponse struct {
	ID int `fesl:"ID"`
}

func PresenceSet(c *router.Context) error {
	var req rgetRequest
	if err := c.Bind(&req); err != nil {
		return err
	}
	return c.Reply(psetResponse{
		ID: req.ID,
	})
}
