// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package routes

import (
	"log"

	"gitlab.com/sparkserver/carbonara/cmd/fesld/routes/acct"
	"gitlab.com/sparkserver/carbonara/cmd/fesld/routes/fsys"
	"gitlab.com/sparkserver/carbonara/cmd/fesld/routes/mtrx"
	"gitlab.com/sparkserver/carbonara/cmd/fesld/routes/rank"
	"gitlab.com/sparkserver/carbonara/cmd/fesld/state"
	"gitlab.com/sparkserver/carbonara/repo"
	"gitlab.com/sparkserver/carbonara/router"
)

func NewRouter(repo repo.Repository) *router.Router {
	r := router.NewRouter(router.CategoryExtractor)
	state := state.ClientState{}

	r.AddRoute("fsys", fsys.NewRouter().Handle)
	r.AddRoute("acct", acct.NewRouter(repo, &state).Handle)
	r.AddRoute("mtrx", mtrx.NewRouter().Handle)
	r.AddRoute("rank", rank.NewRouter(repo).Handle)
	r.AddRoute("dobj", (&DobjCategory{}).Router().Handle)

	r.SetUnhandled(func(c *router.Context) error {
		log.Printf("Unknown message category: %v", c.Message.CategoryString())
		return nil
	})
	return r
}
