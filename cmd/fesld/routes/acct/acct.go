// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package acct

import (
	"crypto/rand"
	"database/sql"
	"encoding/hex"
	"io/ioutil"
	"log"

	"github.com/alexedwards/argon2id"
	"gitlab.com/sparkserver/carbonara/cmd/fesld/state"
	"gitlab.com/sparkserver/carbonara/codec"
	"gitlab.com/sparkserver/carbonara/models"
	"gitlab.com/sparkserver/carbonara/repo"
	"gitlab.com/sparkserver/carbonara/router"
)

func NewRouter(repo repo.Repository, state *state.ClientState) *router.Router {
	c := &AcctCategory{Repo: repo, State: state}
	r := router.NewRouter(router.FieldExtractor("TXN"))
	r.AddRoute("GetCountryList", c.GetCountryList)
	r.AddRoute("GetTos", c.GetTos)
	r.AddRoute("AddAccount", c.AddAccount)
	r.AddRoute("Login", c.Login)
	r.SetUnhandled(func(c *router.Context) error {
		m := codec.PayloadToMap(c.Message.Payload)
		log.Printf("Unknown TXN: %v", m["TXN"])
		return nil
	})
	return r
}

type AcctCategory struct {
	Repo  repo.Repository
	State *state.ClientState
}

type getCountryListResponse struct {
	TXN         string           `fesl:"TXN"`
	CountryList []models.Country `fesl:"countryList"`
}

func (acct *AcctCategory) GetCountryList(c *router.Context) error {
	return c.Reply(getCountryListResponse{
		TXN:         "GetCountryList",
		CountryList: models.AllCountries,
	})
}

type getTOSResponse struct {
	TXN string `fesl:"TXN"`
	TOS string `fesl:"tos"`
}

func (acct *AcctCategory) GetTos(c *router.Context) error {
	tosPath, err := acct.Repo.GetConfigString(models.ConfigTOSPath)
	if err != nil {
		return err
	}
	if tosPath == "" {
		return c.Reply(getTOSResponse{
			TXN: "GetTOS",
			TOS: "No Terms of Service specified",
		})
	}
	tosBytes, err := ioutil.ReadFile(tosPath)
	if err != nil {
		return err
	}
	return c.Reply(getTOSResponse{
		TXN: "GetTOS",
		TOS: string(tosBytes),
	})
}

type addAccountRequest struct {
	CountryCode string `fesl:"countryCode"`
	Email       string `fesl:"email"`
	Name        string `fesl:"name"`
	Password    string `fesl:"password"`
}

type addAccountResponse struct {
	TXN string
}

func (acct *AcctCategory) AddAccount(c *router.Context) error {
	var req addAccountRequest
	if err := c.Bind(&req); err != nil {
		return err
	}

	hash, err := argon2id.CreateHash(req.Password, argon2id.DefaultParams)
	if err != nil {
		return err
	}

	err = acct.Repo.CreateDriver(&models.Driver{
		Name:     req.Name,
		Email:    req.Email,
		Password: hash,
		Country:  req.CountryCode,
	})
	if err != nil {
		return err
	}

	return c.Reply(addAccountResponse{
		TXN: "AddAccount",
	})
}

type loginRequest struct {
	Name     string `fesl:"name"`
	Password string `fesl:"password"`
}

type loginResponse struct {
	TXN         string `fesl:"TXN"`
	UserID      uint64 `fesl:"userId"`
	ProfileID   uint64 `fesl:"profileId"`
	DisplayName string `fesl:"displayName"`
	LKey        string `fesl:"lkey"`
}

func (acct *AcctCategory) Login(c *router.Context) error {
	var req loginRequest
	if err := c.Bind(&req); err != nil {
		return err
	}

	driver, err := acct.Repo.GetDriverByName(req.Name)
	if err != nil {
		if err == sql.ErrNoRows {
			return c.Reply(models.FESLError{
				TXN:  "Login",
				Code: 122,
			})
		}
		return err
	}

	match, err := argon2id.ComparePasswordAndHash(req.Password, driver.Password)
	if err != nil {
		return err
	}
	if !match {
		return c.Reply(models.FESLError{
			TXN:  "Login",
			Code: 122,
		})
	}

	keyBytes := make([]byte, 8)
	if _, err := rand.Read(keyBytes); err != nil {
		return err
	}
	lkey := hex.EncodeToString(keyBytes)

	err = acct.Repo.DeleteDriverSessions(driver.ID)
	if err != nil {
		return err
	}

	err = acct.Repo.CreateSession(&models.Session{
		LKey:   lkey,
		Driver: driver.ID,
	})
	if err != nil {
		return err
	}

	acct.State.ActiveUser = driver

	return c.Reply(loginResponse{
		TXN:         "Login",
		UserID:      driver.ID,
		ProfileID:   driver.ID,
		DisplayName: driver.Name,
		LKey:        lkey,
	})
}
