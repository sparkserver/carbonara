// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package rank

import (
	"log"

	"gitlab.com/sparkserver/carbonara/codec"
	"gitlab.com/sparkserver/carbonara/models"
	"gitlab.com/sparkserver/carbonara/repo"
	"gitlab.com/sparkserver/carbonara/router"
)

func NewRouter(repo repo.Repository) *router.Router {
	r := router.NewRouter(router.FieldExtractor("TXN"))
	c := rankCategory{Repo: repo}
	r.AddRoute("UpdateStats", c.UpdateStats)
	r.AddRoute("GetStats", c.GetStats)
	r.SetUnhandled(func(c *router.Context) error {
		m := codec.PayloadToMap(c.Message.Payload)
		log.Printf("Unknown TXN: %v", m["TXN"])
		return nil
	})
	return r
}

type rankCategory struct {
	Repo repo.Repository
}

type txnResponse struct {
	TXN string `fesl:"TXN"`
}

type updateStatsRequest struct {
	Updates []struct {
		Owner     uint64 `fesl:"o"`
		OwnerType int    `fesl:"ot"`
		Stats     []struct {
			UType int     `fesl:"ut"`
			Key   string  `fesl:"k"`
			Value float32 `fesl:"v"`
			Text  string  `fesl:"t"`
		} `fesl:"s"`
	} `fesl:"u"`
}

func (rank *rankCategory) UpdateStats(c *router.Context) error {
	var req updateStatsRequest
	if err := c.Bind(&req); err != nil {
		return err
	}
	for _, update := range req.Updates {
		dbStats := make([]models.Stat, len(update.Stats))
		for i, gameStat := range update.Stats {
			dbStats[i] = models.Stat{
				Driver: update.Owner,
				Key:    gameStat.Key,
				Value:  gameStat.Value,
				Text:   gameStat.Text,
			}
		}
		if err := rank.Repo.UpdateStats(dbStats); err != nil {
			return err
		}
	}
	return c.Reply(txnResponse{
		TXN: "UpdateStats",
	})
}

type getStatsRequest struct {
	OwnerType  int      `fesl:"owner"`
	PeriodID   int      `fesl:"periodId"`
	PeriodPast int      `fesl:"periodPast"`
	Keys       []string `fesl:"keys"`
	Owner      uint64   `fesl:"owner"`
}

type statsType struct {
	Key   string  `fesl:"key"`
	Value float32 `fesl:"value"`
}

type getStatsResponse struct {
	TXN   string      `fesl:"TXN"`
	Stats []statsType `fesl:"stats"`
}

func (rank *rankCategory) GetStats(c *router.Context) error {
	var req getStatsRequest
	if err := c.Bind(&req); err != nil {
		return err
	}
	dbStats, err := rank.Repo.GetDriverStatsByKeys(req.Owner, req.Keys)
	if err != nil {
		return err
	}
	stats := make([]statsType, len(req.Keys))
outer:
	for i, key := range req.Keys {
		for _, stat := range dbStats {
			if key == stat.Key {
				stats[i] = statsType{
					Key:   key,
					Value: stat.Value,
				}
				// go to next key
				continue outer
			}
		}
		// not found from db response, return 0
		stats[i] = statsType{
			Key:   key,
			Value: 0,
		}
	}
	return c.Reply(getStatsResponse{
		TXN:   "GetStats",
		Stats: stats,
	})
}
