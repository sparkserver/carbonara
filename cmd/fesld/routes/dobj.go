// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package routes

import (
	"log"

	"gitlab.com/sparkserver/carbonara/codec"
	"gitlab.com/sparkserver/carbonara/router"
)

type DobjCategory struct{}

func (d *DobjCategory) Router() *router.Router {
	r := router.NewRouter(router.FieldExtractor("TXN"))
	r.AddRoute("GetObjectInventory", d.GetObjectInventory)

	r.SetUnhandled(func(c *router.Context) error {
		m := codec.PayloadToMap(c.Message.Payload)
		log.Printf("Unknown TXN: %v", m["TXN"])
		return nil
	})
	return r
}

type getObjectInventoryResponse struct {
	TXN          string `fesl:"TXN"`
	Entitlements string `fesl:"entitlements"`
}

func (d *DobjCategory) GetObjectInventory(c *router.Context) error {
	return c.Reply(getObjectInventoryResponse{
		TXN: "GetObjectInventory",
	})
}
