// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package fsys

import (
	"log"

	"gitlab.com/sparkserver/carbonara/codec"
	"gitlab.com/sparkserver/carbonara/router"
)

func NewRouter() *router.Router {
	r := router.NewRouter(router.FieldExtractor("TXN"))
	r.AddRoute("Hello", Hello)
	r.SetUnhandled(func(c *router.Context) error {
		m := codec.PayloadToMap(c.Message.Payload)
		log.Printf("Unknown TXN: %v", m["TXN"])
		return nil
	})
	return r
}

type domainPartition struct {
	Domain    string `fesl:"domain"`
	SubDomain string `fesl:"subDomain"`
}

type helloResponse struct {
	TXN                 string          `fesl:"TXN"`
	MessengerIP         string          `fesl:"messengerIp"`
	MessengerPort       int             `fesl:"messengerPort"`
	DomainPartition     domainPartition `fesl:"domainPartition"`
	ActivityTimeoutSecs int             `fesl:"activityTimeoutSecs"`
	TheaterIP           string          `fesl:"theaterIp"`
	TheaterPort         int             `fesl:"theaterPort"`
}

type memCheckResponse struct {
	TXN      string `fesl:"TXN"`
	Memcheck []struct{}
	Type     int
	Salt     int
}

func Hello(c *router.Context) error {
	err := c.Reply(helloResponse{
		TXN:                 "Hello",
		MessengerIP:         "127.0.0.1",
		MessengerPort:       13505,
		DomainPartition:     domainPartition{"eagames", "NFS-2007"},
		ActivityTimeoutSecs: 0,
		TheaterIP:           "127.0.0.1",
		TheaterPort:         18215,
	})
	if err != nil {
		return err
	}
	return c.Reply(memCheckResponse{
		TXN: "MemCheck",
	})
}
