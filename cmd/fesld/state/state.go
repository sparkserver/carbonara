// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package state

import "gitlab.com/sparkserver/carbonara/models"

type ClientState struct {
	ActiveUser *models.Driver
}
