// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package main

import (
	"crypto/tls"
	"log"
	"net"
	"time"

	"gitlab.com/sparkserver/carbonara/cmd/fesld/routes"
	"gitlab.com/sparkserver/carbonara/codec"
	"gitlab.com/sparkserver/carbonara/config"
	"gitlab.com/sparkserver/carbonara/fesl"
	"gitlab.com/sparkserver/carbonara/models"
	"gitlab.com/sparkserver/carbonara/repo"
	"gitlab.com/sparkserver/carbonara/router"

	_ "github.com/go-sql-driver/mysql"
)

func main() {
	config, err := config.LoadConfig()
	if err != nil {
		log.Fatalf("Failed to open config: %v", err)
	}

	repo, err := repo.NewFromConfig(config)
	if err != nil {
		log.Fatalf("Failed to open DB: %v", err)
	}

	listener, err := net.Listen("tcp", config.Addr)
	if err != nil {
		log.Fatalf("Failed to listen: %v", err)
	}

	keyPair, err := tls.LoadX509KeyPair("test-cert.pem", "test-key.pem")
	if err != nil {
		log.Fatalf("Failed to load keypair: %v", err)
	}

	tlsConfig := &tls.Config{
		Certificates: []tls.Certificate{keyPair},
		CipherSuites: []uint16{
			tls.TLS_RSA_WITH_RC4_128_SHA,
		},
		MinVersion: tls.VersionSSL30,
	}

	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Fatalf("Failed to accept: %v", err)
		}
		tlsConn := tls.Server(conn, tlsConfig)
		go handleConn(repo, tlsConn)
	}
}

func handleConn(repo repo.Repository, tlsConn *tls.Conn) {
	r := routes.NewRouter(repo)
	conn := &fesl.MultipartConn{fesl.SimpleConn{tlsConn}}
	for {
		tlsConn.SetReadDeadline(time.Now().Add(120 * time.Second))
		msg, err := conn.ReadFesl()
		if err != nil {
			if v, ok := err.(net.Error); ok {
				if v.Timeout() {
					sendPing(conn)
					continue
				}
			}
			log.Printf("Failed to read a message: %v", err)
			return
		}
		log.Printf("IN: %#v", msg)

		ctx := &router.Context{
			Message: msg,
			Conn:    conn,
		}
		if err := r.Handle(ctx); err != nil {
			log.Printf("Error handling request: %v", err)
			ctx.Reply(models.FESLError{
				Code: 99,
			})
		}
	}
}

func sendPing(conn fesl.Conn) error {
	pl, err := codec.Marshal(struct {
		TXN string
	}{
		TXN: "Ping",
	})
	if err != nil {
		return err
	}
	return conn.WriteFesl(&fesl.Message{
		Category: [4]byte{0x66, 0x73, 0x79, 0x73}, // fsys
		Seq:      0,
		Payload:  pl,
	})
}
