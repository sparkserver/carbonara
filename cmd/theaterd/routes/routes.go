// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package routes

import (
	"log"
	"time"

	"gitlab.com/sparkserver/carbonara/router"
)

func NewRouter() *router.Router {
	r := router.NewRouter(router.CategoryExtractor)
	r.AddRoute("CONN", Connect)
	r.AddRoute("USER", Login)
	r.AddRoute("LLST", ListLobbies)

	r.SetUnhandled(func(c *router.Context) error {
		log.Printf("Unknown message category: %v", c.Message.CategoryString())
		return nil
	})
	return r
}

type connectResponse struct {
	Time                int64 `fesl:"TIME"`
	ActivityTimeoutSecs int   `fesl:"activityTimeoutSecs"`
	Prot                int   `fesl:"PROT"`
	TID                 int   `fesl:"TID"`
}

func Connect(c *router.Context) error {
	return c.Reply(connectResponse{
		Time:                time.Now().Unix(),
		ActivityTimeoutSecs: 900,
		Prot:                2,
		TID:                 1,
	})
}

type loginResponse struct {
	Name string `fesl:"NAME"`
	TID  int    `fesl:"TID"`
}

func Login(c *router.Context) error {
	return c.Reply(loginResponse{
		Name: "yolo",
		TID:  2,
	})
}

type lobbyListResponse struct {
	NumLobbies int `fesl:"NUM-LOBBIES"`
	TID        int `fesl:"TID"`
}

func ListLobbies(c *router.Context) error {
	return c.Reply(lobbyListResponse{
		NumLobbies: 0,
		TID:        3,
	})
}
