module gitlab.com/sparkserver/carbonara

go 1.12

require (
	github.com/alexedwards/argon2id v0.0.0-20190612080829-01a59b2b8802
	github.com/go-sql-driver/mysql v1.5.0
	github.com/jmoiron/sqlx v1.2.1-0.20200324155115-ee514944af4b
	github.com/stretchr/testify v1.5.1
	google.golang.org/appengine v1.6.5 // indirect
)
